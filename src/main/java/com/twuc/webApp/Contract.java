package com.twuc.webApp;

public class Contract {
    private String name;
    private String id;

    public Contract() {
    }
    public Contract(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }
}
