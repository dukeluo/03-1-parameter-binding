package com.twuc.webApp;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class Person {

    @Email
    private String email;

    @Size(max = 3)
    private String name;

    @Max(9999)
    @Min(1000)
    private Integer yearOfBirth;


    public Person(String name, Integer yearOfBirth, String email) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.email = email;
    }

    public Person() {
    }

    public String getName() {
        return name;
    }

    public Integer getYearOfBirth() {
        return yearOfBirth;
    }

    public String getEmail() {
        return email;
    }
}
