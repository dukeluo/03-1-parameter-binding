package com.twuc.webApp;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class DateTimeLocal {
    @NotNull
    private LocalDate dateTime;

    public DateTimeLocal() {
    }

    public DateTimeLocal(LocalDate dateTime) {
        this.dateTime = dateTime;
    }

    public LocalDate getDateTime() {
        return dateTime;
    }
}
