package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
public class Controller {

    // 2.1.1
    @GetMapping("/api/users/{userId}")
    public String getUserWithIntegerId(@PathVariable Integer userId) {
        return String.format("The id is %d", userId);
    }

    // 2.1.2
    @GetMapping("/api/1/users/{userId}")
    public String getUserWithIntegerId(@PathVariable int userId) {
        return String.format("The id is %d", userId);
    }

    // 2.1.3
    @GetMapping("/api/users/{userId}/books/{bookId}")
    public String getUserWithUserIdAndBookId(@PathVariable int userId, @PathVariable int bookId) {
        return String.format("This is user is is %d, and the book id is %d", userId, bookId);
    }

    // 2.2.1
    @GetMapping("/api/users")
    public String getUserById(@RequestParam int id) {
        return String.format("This is user %d", id);
    }

    // 2.2.2
    @GetMapping("/api/2/users")
    public String getUserByIdWithDefaultId(@RequestParam(defaultValue = "1024") Integer id) {
        return String.format("This is user %d", id);
    }

    // 2.2.3
    @GetMapping("/api/3/users")
    public String getUserByIdCollection(@RequestParam List<String> ids) {
        return ids.toString();
    }

    // 2.3.1
    public String toJson(Contract contract) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(contract);
    }

    // 2.3.1
    public Contract toObject(String input) throws IOException {
        return new ObjectMapper().readValue(input, Contract.class);
    }

    // 2.3.2, 2.4.1
    @PostMapping("/api/datetimes")
    public String getDate(@RequestBody @Valid DateTimeLocal date) {
        return date.getDateTime().toString();
    }

    // 2.4.2
    @PostMapping("/api/person")
    public Person getPerson(@RequestBody @Valid Person person) {
        return person;
    }
}
