package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class ControllerTest {
    // 2.3.1
    @Test
    void should_jsonify_the_contract_object() throws JsonProcessingException {
        Contract contract = new Contract("jack", "1366");
        Controller controller = new Controller();

        assertEquals("{\"name\":\"jack\",\"id\":\"1366\"}", controller.toJson(contract));
    }

    // 2.3.1
    @Test
    void should_get_an_object_from_the_json() throws IOException {
        Controller controller = new Controller();

        assertEquals("jack", controller.toObject("{\"name\":\"jack\",\"id\":\"13666666666\"}").getName());
    }
}