package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class ControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    // 2.1.1
    @Test
    void should_get_user_with_Integer_type_id() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/23"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The id is 23"));
    }

    // 2.1.2
    @Test
    void should_get_user_with_int_type_id() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/23"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The id is 23"));
    }

    // 2.1.3
    @Test
    void should_get_user_with_int_type_user_id_and_book_id() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/64/books/23"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("This is user is is 64, and the book id is 23"));
    }

    // 2.2.1
    @Test
    void should_prove_binding_the_query_parameter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users?id=1024"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("This is user 1024"));
    }

    // 2.2.2
    @Test
    void should_prove_binding_the_query_parameter_with_default_value() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users"))
                .andExpect(MockMvcResultMatchers.status().is(400));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/2/users"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("This is user 1024"));
    }

    // 2.2.3
    @Test
    void should_prove_binding_the_query_parameter_with_a_collection() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/3/users?ids=1,2,3"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("[1, 2, 3]"));
    }

    // 2.3.2
    @Test
    void should_get_data_time_object() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{ \"dateTime\": \"2019-10-01T10:00:00Z\" }"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("2019-10-01"));
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{ \"dateTime\": \"2019-10-01\" }"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("2019-10-01"));
    }

    // 2.4.1
    @Test
    void should_valid_the_param() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{ \"dateTime\": \"2019-10-01T10:00:00Z\" }"))
                .andExpect(MockMvcResultMatchers.status().is(200));
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{ \"dateTime\": null }"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    // 2.4.2
    @Test
    void should_valid_the_param_with_the_size() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/person")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content("{ \"name\": \"jack\" }"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    // 2.4.3
    @Test
    void should_valid_the_param_with_the_min_and_max() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/person")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content("{ \"yearOfBirth\": \"666\" }"))
                .andExpect(MockMvcResultMatchers.status().is(400));
        mockMvc.perform(MockMvcRequestBuilders.post("/api/person")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content("{ \"yearOfBirth\": \"1024\" }"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    // 2.4.4
    @Test
    void should_valid_the_param_with_the_email() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/person")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content("{ \"email\": \"jack#xyz.com\" }"))
                .andExpect(MockMvcResultMatchers.status().is(400));
        mockMvc.perform(MockMvcRequestBuilders.post("/api/person")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content("{ \"email\": \"jack@xyz.com\" }"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
}